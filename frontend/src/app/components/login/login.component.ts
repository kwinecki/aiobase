import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/user';
import {AppComponent} from '../../app.component';
import {JwtHelper} from 'angular2-jwt';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private router: Router, private auth: AuthService, private appComponent: AppComponent) {
  }

  ngOnInit() {
    this.appComponent.setTitle("Login");
    this.appComponent.setKeyword("No keywords provided");
  }

  onLogin(): void {
    this.auth.login(this.user)
      .then((user) => {
        localStorage.setItem('token', user.json().auth_token);

        var decoded_token = this.jwtHelper.decodeToken(user.json().auth_token);

        console.log(decoded_token);

        this.appComponent.isLoggedIn = true;
        this.appComponent.username = decoded_token['username'];
        this.router.navigateByUrl('/');
      })
      .catch((err) => {
      });
  }
}
