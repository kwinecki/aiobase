const ContentToolsLib = require('../../../../assets/content-tools.js');

import {
  NgModule, ViewChild, EventEmitter, AfterViewInit,
  ElementRef, Component, Output, Input, OnDestroy, OnChanges
} from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-content-tools',
  template: '<div #contenttools class="content-tools"><ng-content></ng-content></div>',
})

@NgModule({
  declarations: [ContentToolComponent],
  exports: [ContentToolComponent]
})

export class ContentToolComponent implements AfterViewInit, OnDestroy, OnChanges {
  @Output() onSave = new EventEmitter<Object>();
  @Input() editorState;
  @ViewChild('contenttools') element: ElementRef;
  editor = ContentToolsLib.EditorApp.get();

  test(){
    this.editor.init('*[data-editable]', 'data-name');
  }

  ngAfterViewInit() {
  console.log("test");
    this.editor.init('*[data-editable]', 'data-name');
    const that = this;

    this.editor.addEventListener('saved', function (ev) {
      const regions = ev.detail().regions;
      if (Object.keys(regions).length === 0) {
        return;
      }
      that.onSave.emit(regions);
    });

    this.editor.addEventListener('start', function (ev) {
      const that = this;
      function autoSave() {
        that.save(true);
      };
      this.autoSaveTimer = setInterval(autoSave, 5 * 1000);
    });
  }

  ngOnChanges() {
    if (this.editorState) {
      const state = this.editor.getState();
      const domRegions = this.editor.domRegions();

      console.log('state', state);
      console.log('doms', domRegions);

      this.editor.syncRegions('*[data-editable]', true);
      this.editorState = false;
    }
  }

  ngOnDestroy() {
    this.editor.destroy();
  }
}