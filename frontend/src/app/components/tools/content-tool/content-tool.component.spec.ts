import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentToolComponent } from './content-tool.component';

describe('ContentToolComponent', () => {
  let component: ContentToolComponent;
  let fixture: ComponentFixture<ContentToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
