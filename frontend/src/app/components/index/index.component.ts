import {Component, OnChanges} from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnChanges {

  constructor() {
  }

  ngOnChanges() {
    console.log("index view");
  }

}
