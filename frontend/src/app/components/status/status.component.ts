import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {LogoutRedirect} from '../../services/logout-redirect.service';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})


export class StatusComponent implements OnInit {
  isLoggedIn: boolean = false;

  constructor(private auth: AuthService, private logoutRedirect: LogoutRedirect, private appComponent: AppComponent) {
  }

  ngOnInit() {
    const token = localStorage.getItem('token');
    this.appComponent.setTitle("User Page");
    if (token) {
      this.auth.ensureAuthenticated(token)
        .then((user) => {
          if (user.json().status === 'success') {
            this.isLoggedIn = true;
          }
          else {
            localStorage.clear();
            this.isLoggedIn = false;
            user = null;
          }
        })
        .catch((err) => {
          localStorage.clear();
          this.isLoggedIn = false;
        });
    }
  }

  deleteUser() {
    this.logoutRedirect.execute()
  }

}

