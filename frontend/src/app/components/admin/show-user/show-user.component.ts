import {Component, OnInit} from '@angular/core';
import {UserShort} from '../../../models/user_view';
import {AdminGetUsersService} from '../../../services/admin-get-users.service';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class AdminShowUserComponent implements OnInit {
  users: UserShort[]

  constructor(private userService: AdminGetUsersService) {

  }

  ngOnInit() {

    this.userService.get_users_list(10)
      .then((getData) => {
        this.users = getData.json()['data'];
      })
      .catch((err) => {
        this.users = [{'email': 'failed to obtain data', 'username': 'model users', 'role': err}]
      });

  }

}
