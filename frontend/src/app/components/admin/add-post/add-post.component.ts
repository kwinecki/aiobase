import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Post} from '../../../models/post';
import {AdminPostsService} from '../../../services/admin-posts.service';
import {Router} from '@angular/router';
const ContentToolsLib = require('assets/content-tools.js');

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AdminAddPostComponent implements OnInit, OnDestroy  {
  
  mypost : Post = new Post();  
  private date;
  private  editor = ContentToolsLib.EditorApp.get();
  private _this : Object;
  constructor(private router: Router, private adminPosts : AdminPostsService) {
  
    this.date =  new Date();
   
    let current_time = this.date.toISOString();
    let pos = current_time.lastIndexOf(':');
    current_time = current_time.substring(0,pos);
    
    
    this.mypost.creation_date = current_time;
    this.mypost.published_from_date = current_time;
    this.mypost.published_to_date = current_time;
    this.mypost.show = 1;
  }

  onSave(ev) : AdminAddPostComponent {
  
  	this.mypost.author = ev['inputs']['author'];
    this.mypost.title = ev['inputs']['title'];
    this.mypost.show = ev['inputs']['show'];
    this.mypost.creation_date = ev['inputs']['creation_date'];
    this.mypost.published_from_date = ev['inputs']['published_from_date'];
    this.mypost.published_to_date = ev['inputs']['published_to_date'];
    this.mypost.tags = ev['inputs']['tags'];    
    this.mypost.content = ev['contentTool']['main-content'];       
    this.mypost.pinned = ev['inputs']['pinned'];


        console.log(this.mypost);
   this.adminPosts.save(this.mypost)
      .then((mypost) => {
        console.log("saved");
      })
      .catch((err) => {
      });
      
      return this
  }



  ngOnInit(){
  
	var tmp = this;
	window.addEventListener('load', function() {

	});
	this.editor.init('*[data-editable]', 'data-name');
	this.editor.addEventListener('saved',  function (ev) {
    // Save the changes ...
    var regions = {'contentTool': ev.detail().regions, 'inputs': tmp.mypost};
    tmp.onSave(regions);
    
});
}
	
	
	ngOnDestroy() {
    	this.editor.destroy();
  	}

}
