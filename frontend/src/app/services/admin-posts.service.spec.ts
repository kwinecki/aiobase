import {inject, TestBed} from '@angular/core/testing';

import {AdminPostsService} from './admin-posts.service';

describe('AdminPostsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminPostsService]
    });
  });

  it('should be created', inject([AdminPostsService], (service: AdminPostsService) => {
    expect(service).toBeTruthy();
  }));
});
