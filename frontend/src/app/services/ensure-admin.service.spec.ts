import {inject, TestBed} from '@angular/core/testing';

import {EnsureAdminService} from './ensure-admin.service';

describe('EnsureAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnsureAdminService]
    });
  });

  it('should be created', inject([EnsureAdminService], (service: EnsureAdminService) => {
    expect(service).toBeTruthy();
  }));
});
