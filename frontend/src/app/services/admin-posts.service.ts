import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {Post} from '../models/post';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class AdminPostsService {
  private BASE_URL: string = 'http://localhost:9001/api';

  constructor(private http: Http) {
  }

  save(post: Post): Promise<any> {
    const token = localStorage.getItem('token');
    let headers: Headers = new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    });

    let url: string = `${this.BASE_URL}/admin/save_post`;
    return this.http.post(url, post, {headers: headers}).toPromise();
  }
}
