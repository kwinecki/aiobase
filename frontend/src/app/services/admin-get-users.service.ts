import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AdminGetUsersService {
  private BASE_URL: string = 'http://localhost:9001/api';

  constructor(private http: Http) {
  }

  get_users_list(num): Promise<any> {

    const token = localStorage.getItem('token');
    let headers: Headers = new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    });


    let url: string = `${this.BASE_URL}/admin/get_users`;
    return this.http.get(url, {params: {"num": num}, headers: headers}).toPromise();
  }
}

