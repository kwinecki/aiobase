import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {User} from '../models/user';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {
  private BASE_URL: string = 'http://localhost:9001/api';
  private headers: Headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  login(user: User): Promise<any> {
    let url: string = `${this.BASE_URL}/login`;
    return this.http.post(url, user).toPromise();
  }

  logut(user: User): Promise<any> {
    let url: string = `/logout`;
    return this.http.post(url, user).toPromise();
  }

  register(user: User): Promise<any> {
    let url: string = `${this.BASE_URL}/register`;
    return this.http.post(url, user).toPromise();
  }

  ensureAuthenticated(token): Promise<any> {
    let url: string = `${this.BASE_URL}/status`;
    let headers: Headers = new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    });
    return this.http.get(url, {headers: headers}).toPromise();
  }
}
