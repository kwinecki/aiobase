import {inject, TestBed} from '@angular/core/testing';

import {LogoutRedirectService} from './logout-redirect.service';

describe('LogoutRedirectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogoutRedirectService]
    });
  });

  it('should be created', inject([LogoutRedirectService], (service: LogoutRedirectService) => {
    expect(service).toBeTruthy();
  }));
});
