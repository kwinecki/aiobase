import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';
import {AppComponent} from '../app.component';


@Injectable()
export class LogoutRedirect {
  constructor(private auth: AuthService, private router: Router, private app: AppComponent) {
  }

  execute(): boolean {
    localStorage.clear();
    this.app.deleteUser()
    this.router.navigateByUrl('/');
    return true;
  }
}
