import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';
import {JwtHelper} from 'angular2-jwt';

@Injectable()
export class EnsureAdmin implements CanActivate {
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private auth: AuthService, private router: Router) {
  }

  canActivate(): boolean {
    var token = localStorage.getItem('token')
    if (token) {

      if (this.jwtHelper.isTokenExpired(token)) {
        this.router.navigateByUrl('/login');
        return false;
      }

      var decoded_token = this.jwtHelper.decodeToken(token);

      if (decoded_token['role'] != 2) {
        this.router.navigateByUrl('/login');
        return false;
      }

      return true;
    }
    else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
