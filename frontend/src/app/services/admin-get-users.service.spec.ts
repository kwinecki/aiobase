import {inject, TestBed} from '@angular/core/testing';

import {AdminGetUsersService} from './admin-get-users.service';

describe('AdminGetUsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminGetUsersService]
    });
  });

  it('should be created', inject([AdminGetUsersService], (service: AdminGetUsersService) => {
    expect(service).toBeTruthy();
  }));
});
