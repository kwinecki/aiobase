import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {AuthService} from './services/auth.service';
import {RegisterComponent} from './components/register/register.component';
import {StatusComponent} from './components/status/status.component';
import {EnsureAuthenticated} from './services/ensure-authenticated.service';
import {EnsureAdmin} from './services/ensure-admin.service';
import {LoginRedirect} from './services/login-redirect.service';
import {LogoutRedirect} from './services/logout-redirect.service';
import {IndexComponent} from './components/index/index.component';

import {AdminIndexComponent} from './components/admin/index/index.component';
import {AdminAddPostComponent} from './components/admin/add-post/add-post.component';
import {AdminEditPostComponent} from './components/admin/edit-post/edit-post.component';

import {DatepickerModule} from 'angular-mat-datepicker';
import {AdminShowUserComponent} from './components/admin/show-user/show-user.component'


import {Angular2FontawesomeModule} from 'angular2-fontawesome/angular2-fontawesome'
import {AdminGetUsersService} from './services/admin-get-users.service';
import {AdminPostsService} from './services/admin-posts.service';



@NgModule({

  declarations: [
    AppComponent,


    LoginComponent,
    RegisterComponent,
    StatusComponent,
    IndexComponent,

    AdminIndexComponent,
    AdminAddPostComponent,
    AdminEditPostComponent,
    AdminShowUserComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    DatepickerModule,
    Angular2FontawesomeModule,


    RouterModule.forRoot([
      {
        path: '',
        component: IndexComponent
      },
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginRedirect]
      },
      {
        path: 'register',
        component: RegisterComponent,
        canActivate: [LoginRedirect]
      },
      {
        path: 'status',
        component: StatusComponent,
        canActivate:
          [EnsureAuthenticated]
      },
      {
        path: 'merovingian',
        component: AdminIndexComponent,
        canActivate: [EnsureAdmin],
        children: [
          {
            path: 'add-post',
            component: AdminAddPostComponent,
            canActivate: [EnsureAdmin]
          },
          {
            path: 'edit-post',
            component: AdminEditPostComponent,
            canActivate: [EnsureAdmin]
          },
          {
            path: 'show-user',
            component: AdminShowUserComponent,
            canActivate: [EnsureAdmin]
          }

        ]
      }
    ])
  ],
  providers: [
    AuthService,
    EnsureAuthenticated,
    LoginRedirect,
    LogoutRedirect,
    EnsureAdmin,
    AdminGetUsersService,
    AdminPostsService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
