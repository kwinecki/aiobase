import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {AuthService} from './services/auth.service';
import {JwtHelper} from 'angular2-jwt';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLoggedIn: boolean = false;
  username: string = "";
  jwtHelper: JwtHelper = new JwtHelper();
  

  public constructor(private auth: AuthService, private titleService: Title, private metaService: Meta, private router: Router) {
  }

  ngOnInit() {

    const token = localStorage.getItem('token');
    this.setTitle("Home Page");
    if (token) {
      this.auth.ensureAuthenticated(token)
        .then((user) => {
          if (user.json().status === 'success') {

            var decoded_token = this.jwtHelper.decodeToken(token);
            this.isLoggedIn = true;
            this.username = decoded_token['username'];
          }
          else {
            this.deleteUser()
            user = null;
          }
        })
        .catch((err) => {
          this.deleteUser()
        });
    }
  }

  deleteUser() {
    this.isLoggedIn = false;
    this.username = "";

    localStorage.clear();
    
    this.router.navigateByUrl('/');
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  public setKeyword(newKeyword: string) {
    this.metaService.updateTag({name: 'keywords', content: newKeyword});
  }

  public setAuthor(newAuthor: string) {
    this.metaService.updateTag({name: 'author', content: newAuthor});
  }

  public setDescription(newDescription: string) {
    this.metaService.updateTag({name: 'description', content: newDescription});
  }
}
