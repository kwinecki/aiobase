export class Post {
  author: string;
  title: string;
  content: string;
  show: number;
  creation_date : string;
  published_from_date : string;
  published_to_date : string;  
  tags: string;
  pinned: boolean;
}
