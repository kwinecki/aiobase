from controller.admin.posts import AdminPostHandler
from controller.admin.users import AdminUserHandler
from controller.pub_auth import LoginHandler


def setup_routes(app, cors):
    router = app.router

    # LoginHandler Routes ##############################################

    h = LoginHandler()
    # router.add_get('/', h.index, name='index')

    resource = cors.add(app.router.add_resource("/api/login"))
    cors.add(resource.add_route("POST", h.login))

    resource = cors.add(app.router.add_resource("/api/register"))
    cors.add(resource.add_route("POST", h.register))

    resource = cors.add(app.router.add_resource("/api/status"))
    cors.add(resource.add_route("GET", h.status))

    # LoginHandler Routes End ##########################################

    # Admin Routes  ####################################################

    h = AdminUserHandler()
    resource = cors.add(app.router.add_resource("/api/admin/get_users"))
    cors.add(resource.add_route("GET", h.get_users))

    h = AdminPostHandler()
    resource = cors.add(app.router.add_resource("/api/admin/save_post"))
    cors.add(resource.add_route("POST", h.save_post))

    # Admin Routes END #################################################
