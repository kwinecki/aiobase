import logging

import aiohttp_cors
import asyncio
import config.config as config
import sys
import uvloop
from aiohttp import web
from config.config import JWT_SECRET
from routes.setup_routes import setup_routes
from security.authorizations import auth_middleware

DEBUG = True


async def setup_mongo(app, conf, loop):
    mongo = await config.init_mongo(conf, loop)

    async def close_mongo(app):
        mongo.client.close()

    app.on_cleanup.append(close_mongo)
    return mongo


async def init(loop):
    app = web.Application(loop=loop, middlewares=[auth_middleware])
    mongo = await setup_mongo(app, config.MONGO, loop)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    # setup_jinja(app)

    setup_routes(app, cors)
    return app, config.HOST, config.PORT


def main():
    if not DEBUG:
        logging.basicConfig(level=logging.WARNING)
        logger = logging.getLogger('Security Server')
        if JWT_SECRET in ["CHANGE_ME", ""]:
            logger.critical("JWT secret needs to be updated in config file before we can start!")
            sys.exit(-1)
    else:
        logging.basicConfig(level=logging.DEBUG)

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()
    app, host, port = loop.run_until_complete(init(loop))

    web.run_app(app, host=host, port=port)


if __name__ == '__main__':
    main()
