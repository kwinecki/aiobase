import json
from aiohttp import web
from models.user import User as ModelUser
from security.authorizations import admin_required


class AdminUserHandler:
    def __init__(self):
        pass

    @admin_required
    async def get_users(self, request):
        parameters = request.rel_url.query

        userList = ModelUser.get_list_of_users(parameters['num'])

        myuserList = []
        while (await userList.fetch_next):
            document = userList.next_object()

            user = {"username": document['username'], "role": document['role'],
                    "email": document['credentials']['login']}
            myuserList.append(user)

        responseObject = {
            'status': 'success',
            'data': myuserList
        }

        return web.Response(
            status=200, body=(json.dumps(responseObject)))
