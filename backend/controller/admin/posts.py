import json
from aiohttp import web
from models.blog_post import BlogPosts
from security.authorizations import admin_required

import security.schemas


class AdminPostHandler:
    def __init__(self):
        pass

    @admin_required
    async def save_post(self, request):
        post_data = await request.json()

        # add validation of input data

        new_post = BlogPosts()

        print(post_data)

        new_post.content = post_data['content']
        new_post.author = post_data['author']
        new_post.title = post_data['title']
        new_post.show = post_data['show']
                
        new_post.dates['created'] = post_data['creation_date']
        new_post.dates['modification'] = post_data['creation_date']
        new_post.dates['from_date'] = post_data['published_from_date']
        new_post.dates['to_date'] = post_data['published_to_date']        
        
        new_post.tags = post_data['tags']
        new_post.pinned = post_data['pinned']               

        await BlogPosts.insert(new_post, force=True)

        responseObject = {
            'status': 'success',
            'data': ""
        }

        return web.Response(
            status=200, body=(json.dumps(responseObject)))
        