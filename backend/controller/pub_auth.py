import json

import datetime
import security.schemas
from aiohttp import web
from models.user import User as ModelUser
from security.authorizations import Authenticate, login_required, login_forbidden
from security.kangoo import GenereateHash
from security.schemas import user_credentials_schema, user_register_schema
from utils.strings import randomSalt


class LoginHandler:
    def __init__(self):
        pass

    async def index(self, request):
        return {'Error': 'You should not look here'}

    @login_required
    async def status(self, request):

        responseObject = {
            'status': 'success',
            'data': {
                'user_id': request.user,
                'admin': request.role
            }
        }

        return web.Response(
            status=200, body=(json.dumps(responseObject)))

    @login_forbidden
    async def register(self, request):

        post_data = await request.json()

        # check if login, email and password are valid  - security schema           
        passed, error = security.schemas.checkJson(post_data, user_register_schema)
        if not passed:
            return web.Response(
                status=403,
                body=json.dumps(error))

        login = post_data.get('email')
        username = post_data.get('username')
        password = post_data.get('password')

        # check that login is not used (lowerletters)
        if await ModelUser.get_by_login(login):
            return web.Response(
                status=409,
                body=json.dumps({'status': 'login is used'}))

        if await ModelUser.get_by_username(username):
            return web.Response(
                status=409,
                body=json.dumps({'status': 'username is used'}))

        # generate hash and salt        
        salt = randomSalt()
        hash = GenereateHash(password, salt)

        # insert login hash and salt
        user_data = ModelUser()

        user_data.credentials['login'] = login
        user_data.credentials['hash'] = hash
        user_data.credentials['salt'] = salt
        user_data.username = username

        user_data.registred = datetime.datetime.now()

        await ModelUser.insert(user_data, force=True)

        # return web.Response(
        #    status=201,
        #    body=json.dumps({'status': 'success'}))

        # try:
        jwt_token, user = await Authenticate.login_account(login=login,
                                                           password=password)

        user.last_login = datetime.datetime.now()
        await user.update()

        responseObject = {
            'status': 'success',
            'auth_token': jwt_token.decode()
        }

        return web.Response(
            status=200,
            body=json.dumps(responseObject)
        )

    async def login(self, request):

        post_data = await request.json()

        passed, error = security.schemas.checkJson(post_data, user_credentials_schema)
        if not passed:
            return web.Response(
                status=403,
                body=json.dumps(error))

        login = post_data.get('email')
        password = post_data.get('password')

        # try:
        jwt_token, user = await Authenticate.login_account(login=login,
                                                           password=password)

        user.last_login = datetime.datetime.now()
        await user.update()

        responseObject = {
            'status': 'success',
            'auth_token': jwt_token.decode()
        }

        return web.Response(
            status=200,
            body=json.dumps(responseObject)
        )
