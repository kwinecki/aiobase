import motor.motor_asyncio as aiomotor

JWT_SECRET = "SIAŁA BABA MAK"  # randomString(128)
JWT_ALGORITHM = 'HS512'
JWT_EXP_DELTA_SECONDS = 30 * 3600

MONGO = {
    'host': '127.0.0.1',
    'port': '27017',
    'database': "fbw",
    'max_pool_size': '5'
}

HOST = '127.0.0.1'
PORT = 9001


async def init_mongo(conf, loop):
    mongo_uri = "mongodb://{}:{}".format(conf['host'], conf['port'])
    conn = aiomotor.AsyncIOMotorClient(
        mongo_uri,
        maxPoolSize=conf['max_pool_size'],
        io_loop=loop)
    db_name = conf['database']
    return conn[db_name]
