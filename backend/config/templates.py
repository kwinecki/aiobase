import aiohttp_jinja2
import jinja2
import pathlib
import pytz
from dateutil.parser import parse

TEMPLATES_ROOT = pathlib.Path(__file__).parent / '../templates'


def format_datetime(timestamp):
    if isinstance(timestamp, str):
        timestamp = parse(timestamp)
    return timestamp.replace(tzinfo=pytz.utc).strftime('%Y-%m-%d @ %H:%M')


def setup_jinja(app):
    jinja_env = aiohttp_jinja2.setup(
        app, loader=jinja2.FileSystemLoader(str(TEMPLATES_ROOT)))

    jinja_env.filters['datetimeformat'] = format_datetime
    # jinja_env.filters['robo_avatar_url'] = robo_avatar_url
