import string

PL_LETTERS_LOW = ['a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o',
                  'ó', 'p', 'r', 's', 'ś', 't', 'u', 'w', 'y', 'z', 'ź', 'ż']
PL_LETTERS_UP = ['A', 'Ą', 'B', 'C', 'Ć', 'D', 'E', 'Ę', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'Ń', 'O',
                 'Ó', 'P', 'R', 'S', 'Ś', 'T', 'U', 'W', 'Y', 'Z', 'Ź', 'Ż']
PL_ALPHABET = PL_LETTERS_LOW + PL_LETTERS_UP

EN_LETTERS_LOW = list(string.ascii_lowercase)
EN_LETTERS_UP = list(string.ascii_uppercase)
EN_ALPHABET = EN_LETTERS_LOW + EN_LETTERS_UP
