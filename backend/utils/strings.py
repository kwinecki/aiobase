import random
import string
import utils.letters


def randomSalt():
    return randomString(random.randint(127, 149), (string.ascii_letters + string.digits + string.punctuation))


def randomString(length, dictionary=utils.letters.PL_ALPHABET):
    return ''.join(random.choice(dictionary) for _ in range(length))


def generatePassword(numD=3, numL=7):
    listOfChars = list(randomString(numL, utils.letters.PL_ALPHABET) + randomString(numD, string.digits, numD))
    random.shuffle(listOfChars)
    return ''.join(listOfChars)


def nextLetter(char, lang='PL'):
    # do not process if is not a single char
    if len(str(char)) == 1:
        if lang == 'PL':
            i = utils.letters.PL_ALPHABET.index(char)
            if i == len(utils.letters.PL_ALPHABET):
                return utils.letters.PL_ALPHABET[0]
            return utils.letters.PL_ALPHABET[i + 1]

        if lang == 'EN':
            i = utils.letters.EN_ALPHABET.index(char)
            if i == len(utils.letters.EN_ALPHABET):
                return utils.letters.EN_ALPHABET[0]
            return utils.letters.EN_ALPHABET[i + 1]

    return char
