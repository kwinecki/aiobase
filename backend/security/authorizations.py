import json

import jwt
from aiohttp import web
from config.config import JWT_SECRET, JWT_ALGORITHM, JWT_EXP_DELTA_SECONDS
from core.exceptions import ValueCodeError
from datetime import datetime, timedelta
from gettext import gettext
from models.user import User
from security.kangoo import GenereateHash


@web.middleware
async def auth_middleware(request, handler):
    request.user = None
    jwt_token = request.headers.get('authorization', None)
    if jwt_token:
        try:
            auth_token = jwt_token.split(" ")[1]
            payload = jwt.decode(auth_token, JWT_SECRET,
                                 algorithms=[JWT_ALGORITHM])
            request.user = payload['user_id']
            request.role = payload['role']
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            request.user = None
            # login_required should verify if token is still needed
            # for requested resource

    return await handler(request)


def login_required(func):
    def wrapper(handler, request):
        if not request.user:
            return web.Response(body=json.dumps({'message': 'Auth required'}), status=401)
        return func(handler, request)

    return wrapper


def admin_required(func):
    @login_required
    async def wrapper(handler, request):
        if not (int(request.role) == int(User.TYPE_ADMIN)):
            return web.Response(body=json.dumps({'message': 'Insufficient permission1'}), status=401)
        else:
            _user = await User.get_by_pk(request.user)
            try:
                user = User.create(_user)
            except TypeError:
                return web.Response(body=json.dumps({'message': 'Insufficient permission2'}),
                                    status=401)  # should we ban this person?

            if not user.role == User.TYPE_ADMIN:
                return web.Response(body=json.dumps({'message': 'Insufficient permission3'}),
                                    status=401)  # should we ban this person?

        return await func(handler, request)

    return wrapper


def login_forbidden(func):
    def wrapper(handler, request):
        if request.user:
            return web.Response(body=json.dumps({'message': 'Already logged in'}), status=405)
        return func(handler, request)

    return wrapper


class Authenticate(object):
    AUTH_SUCCESS = 1
    AUTH_ERR_NO_USER = -1
    AUTH_ERR_WRONG_PASSWORD = -2
    AUTH_ERR_NO_LOGIN = -3
    AUTH_ERR_NO_PASSWORD = -4

    error_messages = {
        AUTH_ERR_NO_LOGIN: gettext("No login provided"),
        AUTH_ERR_NO_USER: gettext("User does not exists"),
        AUTH_ERR_NO_PASSWORD: gettext("No password provided"),
        AUTH_ERR_WRONG_PASSWORD: gettext("Invalid password provided"),
    }

    @classmethod
    def get_error_msg(cls, error_code):
        return cls.error_messages.get(error_code, None)

    @classmethod
    async def login_account(cls, login=None, password=None):
        if login is None or login == '':
            raise ValueCodeError(Authenticate.AUTH_ERR_NO_LOGIN)
        if password is None or password == '':
            raise ValueCodeError(Authenticate.AUTH_ERR_NO_PASSWORD)
        _user = await User.get_by_login(login)
        try:
            user = User.create(_user)
        except TypeError:
            raise ValueCodeError(Authenticate.AUTH_ERR_NO_USER)

        current_hash = cls.get_hash(password, user.credentials['salt'])

        Authenticate.check_hash(current_hash, user.credentials['hash'])

        payload = {
            'username': str(user.get_username()),
            'role': str(user.get_role()),
            'user_id': str(user.get_pk()),
            'exp': datetime.utcnow() + timedelta(seconds=JWT_EXP_DELTA_SECONDS)
        }
        jwt_token = jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM)

        return jwt_token, user

    @classmethod
    def get_hash(cls, password, salt):
        return GenereateHash(password, salt)

    @classmethod
    def check_hash(cls, hash1, hash2):
        if hash1 != hash2:
            return False
        return True
