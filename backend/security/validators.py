import re
from cerberus import Validator
from urlvalidator import validate_url, validate_email, ValidationError


class Validatorjson(Validator):

    def _validate_type_objectid(self, value):
        if re.match('[a-f0-9]{24}', value):
            return True

    def _validator_isemail(self, field, value):
        try:
            validate_email(value)
        except ValidationError:
            self._error(field, "Invalid Email")

    def _validator_isWWW(self, field, value):
        try:
            validate_url(value)
        except ValidationError:
            self._error(field, "Invalid URL")

    def _validator_isusername(self, field, value):

        # banned list:
        bannedList = ["@dmin", "admin", "admln", "adm1n", "support", "service", "registration"]

        inputName = value.lower()
        if any(x in inputName for x in bannedList):
            self._error(field, "At banned list")
        else:
            if not re.match(r'^[a-z0-9][a-z0-9_\-@:\s]*[a-z0-9]$', inputName):
                self._error(field, "Valid characters a-z A-Z 0-9 _ - : @")

    # validate img PIL  verify and imghdr.what('/tmp/bass')
