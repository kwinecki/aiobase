# -*- coding: utf-8 -*-

from security.validators import Validatorjson

user_credentials_schema = {
    'email': {'type': 'string', 'validator': 'isemail', 'required': True, 'empty': False},
    'password': {'type': 'string', 'required': True, 'empty': False}
}

user_register_schema = {
    'email': {'type': 'string', 'validator': 'isemail', 'required': True, 'empty': False},
    'password': {'type': 'string', 'required': True, 'empty': False},
    'username': {'type': 'string', 'validator': 'isusername', 'required': True, 'empty': False}
}


def checkJson(obj, schema):
    v = Validatorjson(schema)
    retVal = v.validate(obj)
    return retVal, v.errors
