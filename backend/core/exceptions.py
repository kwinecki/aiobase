# -*- coding: utf-8 -*-


class ValueCodeError(ValueError):
    def __init__(self, error_code):
        self.error_code = error_code

    def __repr__(self):
        return "ValueCode Error {error_code}".format(error_code=self.error_code)
