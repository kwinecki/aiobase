import motor
from bson import ObjectId
from pymongo import MongoClient


class MongoObject(object):
    __database__ = None
    __collection__ = None

    _client = None

    def __setattr__(self, key, value):
        if key != '_dirty' and key in self.__dict__:
            self._dirty.add(key)
        self.__dict__[key] = value

    def __getattr__(self, item):
        if item == '_dirty' and item not in self.__dict__:
            self.__dict__[item] = set()
        if item in self.__dict__:
            return self.__dict__[item]
        raise AttributeError("Item %s dont exists in object %s " % (item, self))

    def mark_change(self, field):
        self._dirty.add(field)

    def changed_fields(self):
        return self._dirty

    def get_changed_fields(self):
        data = {}
        for key in self._dirty:
            data[key] = self.__dict__[key]

        return data

    def clean(self):
        self._dirty.clear()

    def update(self, inc=None):
        """
        wykonwywane na obiekcie wyciaga zmienione dane z obiektu oraz _id i wykonuje update_by_pk

        :return:
        """
        if self.changed_fields():
            updated_data = self.get_changed_fields()
            self.clean()

            return self.update_by_pk(self.get_pk(), updated_data, inc)
        elif inc:
            return self.update_by_pk(self.get_pk(), None, inc)

        return False

    @classmethod
    def update_by_pk(cls, pk, updated_data=None, inc=None):
        """
        aktualizuje dane zdefiniowane w slowniku po pk w mongo

        :param pk: str
        :param updated_data: dict
        :return:
        """
        change_set = {}

        if updated_data:
            change_set['$set'] = updated_data

        if inc:
            change_set['$inc'] = inc

        return cls.get_collection().find_and_modify(
            query={'_id': ObjectId(oid=pk)},
            update=change_set,
        )

    @classmethod
    def insert(cls, data, force=False):

        if isinstance(data, cls):
            data.clean()
            data = data.__dict__

        if '_dirty' in data:
            data.pop('_dirty')

        if not force and '_id' in data:
            data.pop('_id')

        return cls.get_collection().insert(data)

    @classmethod
    def sync(cls):
        """
        Dla skryptow backgroundowych mongo synchroniczne

        :return:
        """
        if not MongoObject._client or not isinstance(MongoObject._client, MongoClient):
            pass
            MongoObject._client = MongoClient('mongodb://127.0.0.1/27017')

    @classmethod
    def get_collection(cls):
        if not MongoObject._client:
            MongoObject._client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://127.0.0.1/27017')
        return MongoObject._client[cls.__database__][cls.__collection__]

    @classmethod
    def create(cls, data=None):
        """
        Tworzy dict z polami zdefiniowanymi w klasie atrybut fields i aktualizuje go o dane z data

        :param data: dict
        :return: dict
        """

        mongo_object = cls()
        mongo_object.clean()
        mongo_object.__dict__.update(data)

        return mongo_object

    @classmethod
    def get_client(cls):
        return cls._client
