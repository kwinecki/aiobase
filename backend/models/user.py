from bson import ObjectId
from core.mongo import MongoObject


class User(MongoObject):
    __database__ = "fbw"
    __collection__ = "users"

    # const
    TYPE_BANNED = -4
    TYPE_NORMAL = 1
    TYPE_ADMIN = 2

    TYPE_KEEP_PRIVATE = -4
    TYPE_SHOW_LOGGED = 1
    TYPE_SHOW_PUBLIC = 2

    # fields
    def __init__(self):
        self.credentials = {
            "login": None,
            "salt": None,
            "hash": None
        }
        self.username = None
        self.registred = None
        self.last_login = None
        self.city = None
        self.location = None  # https://docs.mongodb.com/manual/geospatial-queries/
        self.country = None
        self.age = None
        self.role = self.TYPE_NORMAL
        self.show_email = self.TYPE_KEEP_PRIVATE
        self.fav_sport = None
        self.name_first = None
        self.name_last = None
        self.avatar = None
        self.url = None
        self.about_me = None

    def get_pk(self):
        return self._id

    def get_username(self):
        return "{name}".format(name=self.username)

    def get_role(self):
        return "{name}".format(name=self.role)

    def is_admin(self):
        return int(self.type) == self.TYPE_ADMIN

    @classmethod
    def get_by_pk(cls, pk):
        return cls.get_collection().find_one({'_id': ObjectId(pk)})

    @classmethod
    def get_list_of_users(cls, num):
        return cls.get_collection().find().limit(50)

    @classmethod
    def get_by_username(cls, username):
        return cls.get_collection().find_one({'username': username})

    @classmethod
    def get_by_login(cls, login):
        return cls.get_collection().find_one({'credentials.login': login})
