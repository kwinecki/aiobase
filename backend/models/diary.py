from core.mongo import MongoObject


class Diary(MongoObject):
    __database__ = "fbw"
    __collection__ = "diaries"

    # const    
    TYPE_KEEP_PRIVATE = -4
    TYPE_SHOW_LOGGED = 1
    TYPE_SHOW_PUBLIC = 2

    # fields
    def __init__(self):
        self.author = None
        self.dates = {'created': None, 'modification': None, 'from': None, 'to': None}
        self.show = self.TYPE_KEEP_PRIVATE
        self.diary = []
