from core.mongo import MongoObject


class BlogPosts(MongoObject):
    __database__ = "fbw"
    __collection__ = "blog"

    # const    
    TYPE_KEEP_PRIVATE = -4  # admin only
    TYPE_SHOW_PUBLIC = 1
    TYPE_SHOW_LOGGED = 2

    # fields
    def __init__(self):
        self.author = None
        self.dates = {'created': None, 'modification': None, 'from_date': None, 'to_date': None}
        self.show = self.TYPE_SHOW_PUBLIC
        self.title = None
        self.content = None
        self.tags = []
        self.pinned = False
